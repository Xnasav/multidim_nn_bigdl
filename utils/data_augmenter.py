import numpy as np
import cv2
import time

"""
Augmentation class for images with a corresponding float value (e.g. regression tasks)
"""
class DataAugmenter:

    """Creates a new data augmenter for datasets with image data and labels

        :param samples: list of lists of np.arrays representing the different sensor formats
        :type samples: list of list of np.array
        :param labels: list of float values representing the ouput (label) of the data sample at the same index
        :type labels: list of float
        """
    def __init__(self, samples, labels):

        try:
            for n in samples:
                shape = n[0].shape
                if shape[2] > shape[0]:
                    raise ValueError("Order of images should be channel last!")
            self.samples = samples
            self.num_lists = len(samples)
            self.labels = labels
            self.num_samples = len(self.labels)

        except IndexError:
            raise ValueError("Arrays in List should have 3 dimensions!")
        except TypeError:
            raise ValueError("Samples should be list of list of arrays, labels should be list of floats")


    """Flips images horizontally an negates the corresponding label

            :param threshold: only samples which absolute label values are greater than this threshold get flipped
            :type timestamp: float
            :returns: list of augmented samples and their corresponding labels
            :rtype: list (of lists) and list
            """
    def flip(self, threshold=0.005):


        for i in range(self.num_samples):
            if np.abs(self.labels[i]) > threshold:

                images = [l[i] for l in self.samples]

                flipped_images = [cv2.flip(image, 1) for image in images]

                for n in range(self.num_lists):

                    # If image is a one channel image (e.g. depth image), expand dims
                    if (images[n].shape[2] == 1):
                        self.samples[n].append(np.expand_dims(flipped_images[n], axis=2))
                    else:
                        self.samples[n].append(flipped_images[n])

                self.labels.append(self.labels[i] * (-1))

        return self.samples, self.labels





