import numpy as np

import pickle
import cv2
import time
import os
from threading import Thread


"""
Class to load images from local filesystem
The files should be stored with following 
| folder containing bags
    | bag-name:
        | rgb-folder
            | 1.jpg
            | 2.jpg
            ...
        | depth-folder  
            | 1.png
            | 2.png
            ...
    | ...  
"""
class ImageReader:

    """Creates a new ImageReader for rgb und depth images

            :param path: path to the root folder (containing the folders corresponding to the bags)
            :type path: string or unicode
            :param scale_factor: factor to which the images gets scaled down to
            :type scale_factor: float
            """
    def __init__(self, path, scale_factor):
        self.rgb_images = []
        self.depth_images = []
        self.output = []

        self.cmd_dict = {}
        self.cmd_timestamps = []

        self.path = path
        self.scale_factor = scale_factor

    """Reads the actual data from filesystem

            :returns: list of rgb images, list of depth images and list of labels
            :rtype: list of matrices, list of matrices and list of floats 
            """
    def read_data(self):

        # folder containing folders with the corresponding bag names
        for d in os.listdir(self.path):

            bag_path = self.path + d
            image_names = os.listdir(bag_path + "/rgb")
            image_names = [sample.split(".")[0] for sample in image_names]

            with open(bag_path + '/cmd_dict', 'rb') as fp:
                self.cmd_dict = pickle.load(fp)

            self.cmd_timestamps = np.asarray(self.cmd_dict.keys(), dtype=np.int)

            for name in image_names:
                self._helper(bag_path, name)

            print("Finished with folder: " + d)

        return self.rgb_images, self.depth_images, self.output

    def _helper(self, bag_path, name):

        try:
            image = cv2.imread(bag_path + '/rgb/%s.jpg' % name, 1)
            image = cv2.resize(image, (0, 0), fx=self.scale_factor, fy=self.scale_factor)
            self.rgb_images.append(image)
        except:

            print("Something went wrong when reading rgb image")
            return

        try:
            image = cv2.imread(bag_path + '/depth/%s.png' % name, 0)
            image = np.expand_dims(cv2.resize(image, (0, 0), fx=self.scale_factor, fy=self.scale_factor), axis=2)
            self.depth_images.append(image)
        except:
            print("Something went wrong when reading depth image")
            """
            When we are here, depth image is missing, so remove corresponding rgb-image 
            """
            self.rgb_images.pop()
            return



        nearest = str(self._find_nearest_timestamp(name, self.cmd_timestamps))
        self.output.append(float(self.cmd_dict[nearest]))

    def _find_nearest_timestamp(self, image_timestamp, cmd_timestamps):
        idx = (np.abs(cmd_timestamps - int(image_timestamp))).argmin()
        return cmd_timestamps[idx]

"""
Needs refactoring to not produce wrong image pairs due to threading

class _ThreadReader:

    def __init__(self, bag_path, image_names, cmd_dict, cmd_timestamps, scale_factor):
        self.rgb_images = []
        self.depth_images = []
        self.output = []

        self.bag_path = bag_path
        self.image_names = image_names
        self.cmd_dict = cmd_dict
        self.cmd_timestamps = cmd_timestamps
        self.scale_factor = scale_factor

    def read_images(self):
        threads = []

        for name in self.image_names:
            threads.append(Thread(target=self.helper, args=(self.bag_path, name, self.cmd_dict, self.cmd_timestamps)))
            threads[-1].start()

        for thread in threads:
            # Waits for threads to complete before moving on with the main script.
            thread.join()

        return self.rgb_images, self.depth_images, self.output

"""
