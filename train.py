import argparse
import numpy as np



from models.fusenet import FuseNet

from utils.data_utils import ImageReader
from utils.data_augmenter import  DataAugmenter


def main():

    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--folder", dest="root_folder", help="folder in which the image data is stored", default="/root/BigDL/resources/images-short/")
    parser.add_argument("-c", "--classes", dest="num_classes", help="specify the number of output classes", default=1)
    parser.add_argument("-d", "--dropout", dest="dropout", help="True to use dropout or False to not use dropout", default=True)
    parser.add_argument("-b", "--batchnorm", dest="batch_norm", help="True to use batch normalisiation or False to not use it", default=True)
    parser.add_argument("-s", "--scale", dest="scale_factor", help="factor by which the images get scaled down to", default=1)
    parser.add_argument("-t", "--test", dest="test_factor", help="percentage of data to be used as test data",
                        default=0.05)
    args = parser.parse_args()

    # Create data set
    new_reader = ImageReader(args.root_folder, float(args.scale_factor))
    rgb, depth, out = new_reader.read_data()

    print("Num of samples: " + str(len(rgb)))

    new_augmenter = DataAugmenter([rgb, depth], out)
    samples, labels = new_augmenter.flip()
    rgb = samples[0]
    depth = samples[1]



    rgb = np.asarray(rgb)
    depth = np.asarray(depth)
    labels = np.expand_dims(np.asarray(out), axis=1)

    print("Num of samples after augmentation: " + str(rgb.shape[0]))


    rgb_shape = rgb[0].shape
    depth_shape = depth[0].shape
    print (rgb_shape)


    labels = labels * 1
    model = FuseNet(args.num_classes, args.batch_norm, args.dropout)
    model = model.build(rgb_shape, depth_shape)

    test_split = int((args.test_factor * rgb.shape[0])/2)
    print(test_split)
    model.fit(x=[rgb[test_split:-test_split], depth[test_split:-test_split]], y=labels[test_split:-test_split], batch_size=32, nb_epoch=200, distributed=False)

    model.save("trained/fusenet")



    res = model.predict([rgb[-test_split:], depth[-test_split:]])

    np.set_printoptions(threshold='nan')
    print(np.append(res, labels[-test_split:], axis=1))

    res = model.predict([rgb[:test_split], depth[:test_split]])

    np.set_printoptions(threshold='nan')
    print(np.append(res, labels[:test_split], axis=1))



if __name__ == '__main__':
    main()
