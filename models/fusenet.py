from bigdl.nn.keras.layer import Dense, Convolution2D, Flatten, Input, merge, BatchNormalization, MaxPooling2D, Dropout
from bigdl.nn.keras.topology import Model



class FuseNet:
    def __init__(self, numc, batchn, dropout):
        self.num_classes = numc
        self.batch_norm = batchn
        self.drop_out = dropout
        print("Created depthnet model")

    def build(self, rgb_shape, depth_shape):

        #########
        # Depth #
        #########
        # Define depth-branch for fusenet containing samples of greyscale depth images
        depth_input = Input(shape=(depth_shape))

        if self.batch_norm:
            depth_norm = BatchNormalization()(depth_input)
        else:
            depth_norm = depth_input

        depth_1_1 = Convolution2D(32, 3, 3, activation="relu", border_mode="same", dim_ordering='tf')(depth_norm)
        depth_1_2 = Convolution2D(32, 5, 5, activation="relu",  border_mode="same", dim_ordering='tf')(depth_1_1)
        depth_1_3 = Convolution2D(32, 3, 3, activation="relu", border_mode="same", dim_ordering='tf')(depth_1_2)
        #Fusion here
        depth_1_4 = MaxPooling2D(pool_size=(2, 2), strides=(2,2), border_mode='valid')(depth_1_3)

        depth_2_1 = Convolution2D(64, 3, 3, activation='relu', border_mode='same', dim_ordering='tf')(depth_1_4)
        depth_2_2 = Convolution2D(64, 5, 5, activation='relu', border_mode='same', dim_ordering='tf')(depth_2_1)
        depth_2_3 = Convolution2D(64, 3, 3, activation='relu', border_mode='same', dim_ordering='tf')(depth_2_2)
        #Fusion here
        depth_2_4 = MaxPooling2D(pool_size=(2, 2), strides=(2, 2), border_mode='valid')(depth_2_3)

        depth_3_1 = Convolution2D(128, 3, 3, activation='relu', border_mode='same', dim_ordering='tf')(depth_2_4)
        depth_3_2 = Convolution2D(128, 5, 5, activation='relu', border_mode='same', dim_ordering='tf')(depth_3_1)
        depth_3_3 = Convolution2D(128, 3, 3, activation='relu', border_mode='same', dim_ordering='tf')(depth_3_2)
        # Fusion here



        #########
        #  RGB  #
        #########
        # Define rgb-branch for fusenet containing samples of rgb images
        rgb_input = Input(shape=(rgb_shape))

        if self.batch_norm:
            rgb_norm = BatchNormalization()(rgb_input)
        else:
            rgb_norm = rgb_input

        rgb_1_1 = Convolution2D(32, 3, 3, activation="relu", border_mode="same", dim_ordering='tf')(rgb_norm)
        rgb_1_2 = Convolution2D(32, 5, 5, activation="relu", border_mode="same", dim_ordering='tf')(rgb_1_1)
        rgb_1_3 = Convolution2D(32, 3, 3, activation="relu", border_mode="same", dim_ordering='tf')(rgb_1_2)
        add_1_4 = merge(inputs=[rgb_1_3, depth_1_3], mode='sum')
        rgb_1_5 = MaxPooling2D(pool_size=(2, 2), strides=(2, 2), border_mode='valid')(add_1_4)

        if self.batch_norm:
            rgb_norm_2 = BatchNormalization()(rgb_1_5)
        else:
            rgb_norm_2 = rgb_1_5
        rgb_2_1 = Convolution2D(64, 3, 3, activation='relu', border_mode='same', dim_ordering='tf')(rgb_norm_2)
        rgb_2_2 = Convolution2D(64, 5, 5, activation='relu', border_mode='same', dim_ordering='tf')(rgb_2_1)
        rgb_2_3 = Convolution2D(64, 3, 3, activation='relu', border_mode='same', dim_ordering='tf')(rgb_2_2)
        add_2_4 = merge(inputs=[rgb_2_3, depth_2_3], mode='sum')
        rgb_2_5 = MaxPooling2D(pool_size=(2, 2), strides=(2, 2), border_mode='valid')(add_2_4)

        if self.batch_norm:
            rgb_norm_3 = BatchNormalization()(rgb_2_5)
        else:
            rgb_norm_3 = rgb_2_5
        rgb_3_1 = Convolution2D(128, 3, 3, activation='relu', border_mode='same', dim_ordering='tf')(rgb_norm_3)
        rgb_3_2 = Convolution2D(128, 5, 5, activation='relu', border_mode='same', dim_ordering='tf')(rgb_3_1)
        rgb_3_3 = Convolution2D(128, 3, 3, activation='relu', border_mode='same', dim_ordering='tf')(rgb_3_2)
        add_3_4 = merge(inputs=[rgb_3_3, depth_3_3], mode='sum')
        rgb_3_5 = MaxPooling2D(pool_size=(2, 2), strides=(2, 2), border_mode='valid')(add_3_4)


        flatten_4_1 = Flatten()(rgb_3_5)
        dense_4_2 = Dense(64, activation='relu')(flatten_4_1)
        if self.drop_out:
            dense_4_3 = Dropout(0.5)(dense_4_2)
        else:
            dense_4_3 = dense_4_2
        dense_4_4 = Dense(self.num_classes)(dense_4_3)



        model = Model([rgb_input, depth_input], dense_4_4)
        model.compile(loss='mean_absolute_error', optimizer='adadelta', metrics=['accuracy'])

        return model
